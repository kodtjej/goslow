package main

import (
	"math"
	"net/http"
	"sync/atomic"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/koding/multiconfig"
)

var count int64

type config struct {
	Port    string `default:"8080"`
	Sleep   int    `default:"11"`
	Timeout int    `default:"0"`
}

func main() {
	config := config{}
	multiconfig.New().MustLoad(&config)

	r := gin.Default()
	r.GET("/goslow", response(config))
	r.POST("/goslow", response(config))
	r.PUT("/goslow", response(config))
	r.DELETE("/goslow", response(config))

	r.Run(":" + config.Port)
}

func response(c config) gin.HandlerFunc {
	return func(g *gin.Context) {
		time.Sleep(time.Second * time.Duration(c.Sleep))

		atomic.AddInt64(&count, 1)

		// Return timeout error code every "c.Timeout" (default 0th) request
		if math.Mod(float64(atomic.LoadInt64(&count)), float64(c.Timeout)) == 0 {
			g.AbortWithStatus(http.StatusRequestTimeout)
			return
		}

		g.Status(http.StatusOK)
	}
}
