# Goslow or go home
Made to test timeouts and how to handle those things.

## To get goslow
`go get gitlab.com/xonvanetta/goslow`

## Run goslow
`goslow`

## Command flags

Port for the webserver.

Sleep is in seconds. 

Timeout is X:th request, for example every 3rd request.
```
  -port
        Change value of Port. (default 8080)
  -sleep
        Change value of Sleep. (default 11)
  -timeout
        Change value of Timeout. (default 0)

Generated environment variables:
   CONFIG_PORT
   CONFIG_SLEEP
   CONFIG_TIMEOUT
```

`goslow -port 3001 -sleep 60`